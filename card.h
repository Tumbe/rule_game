#ifndef CARD_H
#define CARD_H

#include <iostream>
#include <vector>
#include <QImage>

using namespace std;

class Card
{
public:
    Card();
    void setValue(string value);
    void setRule(string rule);
    void setPicture(string value, string suitValue);

    string getValue();
    string getRule();
    QImage getPic();



private:
    //string suit_;
    string value_;
    string rule_ = "No rule";

    QImage cardPic_;
};

#endif // CARD_H
