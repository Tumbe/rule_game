QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG += qt

SOURCES += \
        card.cpp \
        carddeck.cpp \
        main.cpp \
        mainwindow.cpp \
        ruledialog.cpp

HEADERS += \
    card.h \
    carddeck.h \
    mainwindow.h \
    ruledialog.h

FORMS += \
    mainwindow.ui \
    ruledialog.ui

DISTFILES += \
    cardImages/10C.png \
    cardImages/10D.png \
    cardImages/10H.png \
    cardImages/10S.png \
    cardImages/2C.png \
    cardImages/2D.png \
    cardImages/2H.png \
    cardImages/2S.png \
    cardImages/3C.png \
    cardImages/3D.png \
    cardImages/3H.png \
    cardImages/3S.png \
    cardImages/4C.png \
    cardImages/4D.png \
    cardImages/4H.png \
    cardImages/4S.png \
    cardImages/5C.png \
    cardImages/5D.png \
    cardImages/5H.png \
    cardImages/5S.png \
    cardImages/6C.png \
    cardImages/6D.png \
    cardImages/6H.png \
    cardImages/6S.png \
    cardImages/7C.png \
    cardImages/7D.png \
    cardImages/7H.png \
    cardImages/7S.png \
    cardImages/8C.png \
    cardImages/8D.png \
    cardImages/8H.png \
    cardImages/8S.png \
    cardImages/9C.png \
    cardImages/9D.png \
    cardImages/9H.png \
    cardImages/9S.png \
    cardImages/AC.png \
    cardImages/AD.png \
    cardImages/AH.png \
    cardImages/AS.png \
    cardImages/JC.png \
    cardImages/JD.png \
    cardImages/JH.png \
    cardImages/JS.png \
    cardImages/KC.png \
    cardImages/KD.png \
    cardImages/KH.png \
    cardImages/KS.png \
    cardImages/QC.png \
    cardImages/QD.png \
    cardImages/QH.png \
    cardImages/QS.png \
    cardImages/blue_back.png \
    cardImages/cardBack.jpg \
    cardImages/gray_back.png \
    cardImages/green_back.png \
    cardImages/purple_back.png \
    cardImages/red_back.png \
    cardImages/yellow_back.png
