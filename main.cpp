#include "mainwindow.h"
#include "carddeck.h"
#include "card.h"

#include <QApplication>
#include <iostream>


using namespace std;

int main(int argc, char *argv[])
{
    CardDeck* deck = new CardDeck;

    QApplication a(argc, argv);
    MainWindow w;
    w.addDeck(deck);
    w.show();

    return a.exec();


/*   // Creating the card deck
    CardDeck* deck = new CardDeck();
    for (int i = 0; i < 4; i++ ) {
        for(int j = 1; j <= DECK_SIZE/4; j++) {

            Card* card = new Card();
            if (j == 1){
                card->setValue("A");
            }
            else if (j == 11){
                card->setValue("J");
            }
            else if(j == 12){
                card->setValue("Q");
            }
            else if(j == 13){
                card->setValue("K");
            }

            else {
                card->setValue(std::to_string(j));
            }
            deck->addCard(card);
        }
    }


    for (Card *card :deck->getDeck()){
        cout << card->getValue() << endl;
    }
    deck->shuffle();
    cout << "sekotettu" << endl;
    for (Card *card :deck->getDeck()){
        cout << card->getValue() << endl;
    }

    cout << "" << endl;
    cout << deck->getTopCard()->getValue() << endl;
    deck->deleteCard(deck->getTopCard());
    cout << "" <<endl;

    for (Card *card :deck->getDeck()){
        cout << card->getValue() << endl;
    }


    string rule = "";
    string card1Value = "";
    cout << "Choose a card you would like to set a rule: ";
    getline(cin, card1Value);
    std::transform(card1Value.begin(), card1Value.end(),card1Value.begin(), ::toupper);
    cout << "Set rule for the card: ";
    getline(cin, rule);

    for (Card* card2 : deck->getDeck()) {
        if (card1Value == card2->getValue()) {
            card2->setRule(rule);
        }
    }
    for (Card *card :deck->getDeck()){
        cout << card->getRule() << endl;
    }

*/

}
