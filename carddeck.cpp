#include "carddeck.h"

CardDeck::CardDeck()
{
    for (int i = 0; i < 4; i++ ) {
        for(int j = 1; j <= DECK_SIZE/4; j++) {

            Card* card = new Card();
            // ACE PICTURES AND VALUE
            if (j == 1){
                card->setValue("A");
                card->setPicture("A", std::to_string(i));
            }
            // JACK PICTURES AND VALUE
            else if (j == 11){
                card->setValue("J");
                card->setPicture("J", std::to_string(i));
            }
            // QUEEN PICTURE AND VALUE
            else if(j == 12){
                card->setValue("Q");
                card->setPicture("Q", std::to_string(i));
            }
            // KING PICTURES AND VALUE
            else if(j == 13){
                card->setValue("K");
                card->setPicture("K", std::to_string(i));
            }
            // NUM CARDS PICTURES AND VALUE
            else {
                card->setValue(std::to_string(j));
                card->setPicture(std::to_string(j), std::to_string(i));
            }
            addCard(card);
        }
    }
    shuffle();
}

void CardDeck::shuffle()
{
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    auto rng = std::default_random_engine { seed };
    std::shuffle(std::begin(cards_), std::end(cards_), rng);
}

void CardDeck::deleteCard(Card* card)
{
    cards_.erase(std::remove(cards_.begin(), cards_.end(), card), cards_.end());
}

void CardDeck::addCard(Card* card)
{
    cards_.push_back(card);
}

Card *CardDeck::getTopCard()
{
    return cards_[0];
}

vector<Card *> CardDeck::getDeck()
{
    return cards_;
}

CardDeck::~CardDeck()
{

}
