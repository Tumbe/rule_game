#include "card.h"

Card::Card()
{
}

void Card::setValue(string value)
{
    value_ = value;
}

void Card::setRule(string rule)
{
    rule_ = rule;
}

void Card::setPicture(string value, string suitValue)
{
    if (suitValue == "0"){
        suitValue = "C";
    }
    else if (suitValue == "1"){
        suitValue = "D";
    }
    else if (suitValue == "2"){
        suitValue = "H";
    }
    else if (suitValue == "3"){
        suitValue = "S";
    }
    // ''fullValue'' is .PNG name in the image folder
    QString fullValue = QString::fromUtf8((value + suitValue).c_str());
    cardPic_.load(QString("C:/Users/Tumbe/Documents/omaasettii/juomapeli/rule_game/cardImages/%1.png").arg(fullValue));
}

string Card::getValue()
{
    return value_;
}

string Card::getRule()
{
    return rule_;
}

QImage Card::getPic()
{
    return cardPic_;
}
