#ifndef RULEDIALOG_H
#define RULEDIALOG_H

#include <QString>
#include <QDialog>
#include <QPlainTextEdit>
#include <iostream>
#include <fstream>
#include <unordered_map>

#include "carddeck.h"

namespace Ui {
class RuleDialog;
}

class RuleDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RuleDialog(QWidget *parent = nullptr);
    ~RuleDialog();

    void addDeck(CardDeck *deck);
    void showRules();
    void textToRule(string value, string rule);

private slots:

    void setButton_clicked();
    void saveButton_clicked();
    void loadButton_clicked();

private:
    Ui::RuleDialog *ui;
    CardDeck *deck_;
};

#endif // RULEDIALOG_H
