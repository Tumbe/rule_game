#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPixmap>
#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

#include "card.h"
#include "carddeck.h"
#include "ruledialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void addDeck(CardDeck *deck);

private slots:
    void takeButton_clicked();

    void ruleButton_clicked();

private:
    Ui::MainWindow *ui;
    CardDeck *deck_;
    bool allowDraw_ = false;
};

#endif // MAINWINDOW_H
