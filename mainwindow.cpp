#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QObject::connect(ui->takeButton ,SIGNAL(clicked()),this, SLOT(takeButton_clicked()));
    QObject::connect(ui->ruleButton ,SIGNAL(clicked()),this, SLOT(ruleButton_clicked()));
    QObject::connect(ui->exitButton ,SIGNAL(clicked()),this, SLOT(close()));

    // Card backgroundimage for the deckbutton
    ui->takeButton->setStyleSheet("border-image:url(C:/Users/Tumbe/Documents/omaasettii/juomapeli/hitler/cardImages/blue_back.png);");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addDeck(CardDeck *deck)
{
    deck_ = deck;
}

void MainWindow::takeButton_clicked()
{
    if (!deck_->getDeck().empty() and allowDraw_) {
        Card* topCard = deck_->getTopCard();

        cout << topCard->getValue() << endl;

        /* Set card's picture to the value label
         * and rule text to the rule label*/
        ui->valueLabel->setScaledContents(true);
        ui->valueLabel->setPixmap(QPixmap::fromImage(topCard->getPic()));
        ui->ruleLabel->setText(QString::fromUtf8(topCard->getRule().c_str()));
        deck_->deleteCard(topCard);
    }

    else if (deck_->getDeck().empty() and allowDraw_) {
        // If deck is empty: clear button pic and label pic
        ui->takeButton->setStyleSheet("");
        ui->valueLabel->clear();
        ui->ruleLabel->setText(QString::fromUtf8("No cards left!"));
    }
    else if (!allowDraw_){
        ui->ruleLabel->setText(QString::fromUtf8("Set some rules first"));
    }

}

void MainWindow::ruleButton_clicked()
{
    RuleDialog ruleWindow;
    ruleWindow.setModal(true);
    ruleWindow.addDeck(deck_);
    ruleWindow.showRules();
    allowDraw_ = true;
    ruleWindow.exec();
}
/*
void MainWindow::loadButton_clicked()
{
    ifstream infile;
    string line;
    vector<string> fileRules;
    infile.open("rules.txt");
    while(getline(infile, line)){
        fule
    }


    allowDraw_ = true;
}*/
