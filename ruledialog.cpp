#include "ruledialog.h"
#include "ui_ruledialog.h"

RuleDialog::RuleDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RuleDialog)
{
    ui->setupUi(this);
    QObject::connect(ui->setButton ,SIGNAL(clicked()),this, SLOT(setButton_clicked()));
    QObject::connect(ui->saveButton ,SIGNAL(clicked()),this, SLOT(saveButton_clicked()));
    QObject::connect(ui->loadButton ,SIGNAL(clicked()),this, SLOT(loadButton_clicked()));
}

RuleDialog::~RuleDialog()
{
    delete ui;
}

void RuleDialog::addDeck(CardDeck *deck)
{
    deck_ = deck;
}

void RuleDialog::showRules()
{
    for (Card* card : deck_->getDeck()) {
        if (card->getValue() == "A") {
            ui->aceRule->setText(QString::fromUtf8(card->getRule().c_str()));
        }
        else if (card->getValue() == "2") {
            ui->twoRule->setText(QString::fromUtf8(card->getRule().c_str()));
        }
        else if (card->getValue() == "3") {
            ui->threeRule->setText(QString::fromUtf8(card->getRule().c_str()));
        }
        else if (card->getValue() == "4") {
            ui->fourRule->setText(QString::fromUtf8(card->getRule().c_str()));
        }
        else if (card->getValue() == "5") {
            ui->fiveRule->setText(QString::fromUtf8(card->getRule().c_str()));
        }
        else if (card->getValue() == "6") {
            ui->sixRule->setText(QString::fromUtf8(card->getRule().c_str()));
        }
        else if (card->getValue() == "7") {
            ui->sevenRule->setText(QString::fromUtf8(card->getRule().c_str()));
        }
        else if (card->getValue() == "8") {
            ui->eightRule->setText(QString::fromUtf8(card->getRule().c_str()));
        }
        else if (card->getValue() == "9") {
            ui->nineRule->setText(QString::fromUtf8(card->getRule().c_str()));
        }
        else if (card->getValue() == "10") {
            ui->tenRule->setText(QString::fromUtf8(card->getRule().c_str()));
        }
        else if (card->getValue() == "J") {
            ui->jackRule->setText(QString::fromUtf8(card->getRule().c_str()));
        }
        else if (card->getValue() == "Q") {
            ui->queenRule->setText(QString::fromUtf8(card->getRule().c_str()));
        }
        else if (card->getValue() == "K") {
            ui->kingRule->setText(QString::fromUtf8(card->getRule().c_str()));
        }
    }
}

void RuleDialog::textToRule(string value, string rule)
{
    for (Card* card : deck_->getDeck()) {
        if (card->getValue() == value) {
            card->setRule(rule);
        }
    }
}

void RuleDialog::setButton_clicked()
{
     for (Card* card : deck_->getDeck()) {
        if (card->getValue() == "A") {
            card->setRule(ui->aceRule->text().toUtf8().constData());
        }
        else if (card->getValue() == "2") {
            card->setRule(ui->twoRule->text().toUtf8().constData());
        }
        else if (card->getValue() == "3") {
            card->setRule(ui->threeRule->text().toUtf8().constData());
        }
        else if (card->getValue() == "4") {
            card->setRule(ui->fourRule->text().toUtf8().constData());
        }
        else if (card->getValue() == "5") {
            card->setRule(ui->fiveRule->text().toUtf8().constData());
        }
        else if (card->getValue() == "6") {
            card->setRule(ui->sixRule->text().toUtf8().constData());
        }
        else if (card->getValue() == "7") {
            card->setRule(ui->sevenRule->text().toUtf8().constData());
        }
        else if (card->getValue() == "8") {
            card->setRule(ui->eightRule->text().toUtf8().constData());
        }
        else if (card->getValue() == "9") {
            card->setRule(ui->nineRule->text().toUtf8().constData());
        }
        else if (card->getValue() == "10") {
            card->setRule(ui->tenRule->text().toUtf8().constData());
        }
        else if (card->getValue() == "J") {
            card->setRule(ui->jackRule->text().toUtf8().constData());
        }
        else if (card->getValue() == "Q") {
            card->setRule(ui->queenRule->text().toUtf8().constData());
        }
        else if (card->getValue() == "K") {
            card->setRule(ui->kingRule->text().toUtf8().constData());
        }
    }
     this->close();
}

void RuleDialog::saveButton_clicked()
{
     ofstream outfile ("rules.txt");
     vector<string> rules;
     rules.push_back(ui->aceRule->text().toUtf8().constData());
     rules.push_back(ui->twoRule->text().toUtf8().constData());
     rules.push_back(ui->threeRule->text().toUtf8().constData());
     rules.push_back(ui->fourRule->text().toUtf8().constData());
     rules.push_back(ui->fiveRule->text().toUtf8().constData());
     rules.push_back(ui->sixRule->text().toUtf8().constData());
     rules.push_back(ui->sevenRule->text().toUtf8().constData());
     rules.push_back(ui->eightRule->text().toUtf8().constData());
     rules.push_back(ui->nineRule->text().toUtf8().constData());
     rules.push_back(ui->tenRule->text().toUtf8().constData());
     rules.push_back(ui->jackRule->text().toUtf8().constData());
     rules.push_back(ui->queenRule->text().toUtf8().constData());
     rules.push_back(ui->kingRule->text().toUtf8().constData());


    for (string value: rules) {
        outfile << value << endl;
    }
    outfile.close();
}

void RuleDialog::loadButton_clicked()
{
    ifstream infile;
    string line;
    int i = 1;
    infile.open("rules.txt");

    while(i < 14){
        getline(infile, line);

        if (i == 1) {
            textToRule("A",line);
            ++i;
        }
        else if (i == 11) {
            textToRule("J", line);
            ++i;
        }
        else if(i ==12) {
            textToRule("Q", line);
            ++i;
        }
        else if(i == 13) {
            textToRule("K", line);
            ++i;
        }
        else {
            textToRule(std::to_string(i), line);
            ++i;
        }
    }
    showRules();
}
