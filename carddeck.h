#ifndef CARDDECK_H
#define CARDDECK_H

#include <vector>
#include <iostream>
#include <algorithm>
#include <random>
#include <ctime>
#include <cstdlib>
#include <chrono>

#include "card.h"

using namespace std;

const int DECK_SIZE = 52;

class CardDeck
{
public:
    CardDeck();

    virtual void shuffle();
    virtual void deleteCard(Card* card);
    virtual void addCard(Card* card);

    virtual Card* getTopCard();
    virtual vector<Card*> getDeck();

    ~CardDeck();

private:

    vector<Card*> cards_;

};

#endif // CARDDECK_H
